package com.seatapp.seatapp.repositories;

import org.springframework.data.repository.CrudRepository;

import com.seatapp.seatapp.domain.Todo;

public interface TodoRespository extends CrudRepository<Todo, Long> {

}
