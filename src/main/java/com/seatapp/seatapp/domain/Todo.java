package com.seatapp.seatapp.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
public class Todo {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotBlank
	@Size(max=10)
	private String name;
	
	@Size(max=10)
	private String description;
	
}
