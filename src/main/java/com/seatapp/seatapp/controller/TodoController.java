package com.seatapp.seatapp.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seatapp.seatapp.domain.Todo;
import com.seatapp.seatapp.service.TodoService;

@Controller
@RequestMapping("/todo")
public class TodoController {

	@Autowired
	private TodoService todoService;

	@RequestMapping("/list")
	public String list(Map<String, Object> model) {
		model.put("resultList", todoService.findAllTodo());
		model.put("todo", new Todo());
		return "todo";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@Valid @ModelAttribute("todo") Todo todo, BindingResult bindingResult,Map<String, Object> model) {
		
		if(bindingResult.hasErrors()){
			// TODO do not find all, separate creation and list page  
			model.put("resultList", todoService.findAllTodo());
			return "todo";
		}
		
		todoService.saveTodo(todo);

		return "redirect:/todo/list";
	}

	@RequestMapping("/delete")
	public String delete(@RequestParam Long id) {
		todoService.deleteTodoById(id);

		return "redirect:/todo/list";
	}
}
