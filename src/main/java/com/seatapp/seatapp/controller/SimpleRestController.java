package com.seatapp.seatapp.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableAutoConfiguration
public class SimpleRestController {

	@RequestMapping("/rest")
	String home() {
		return "Hello World!";
	}
}
