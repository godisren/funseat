package com.seatapp.seatapp.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.seatapp.seatapp.domain.Todo;
import com.seatapp.seatapp.service.TodoService;

@Controller
@RequestMapping("/seat")
public class SeatDesignController {

	

	@RequestMapping("/design")
	public String list(Map<String, Object> model) {		
		return "seat_design";
	}

	
}
