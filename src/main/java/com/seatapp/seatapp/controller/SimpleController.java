package com.seatapp.seatapp.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SimpleController {

	private String message = "Hello World";

	@RequestMapping("/test")
	public String welcome(Map<String, Object> model) {
		model.put("message", this.message);
		return "index";
	}
}
