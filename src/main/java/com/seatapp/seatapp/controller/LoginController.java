package com.seatapp.seatapp.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	@RequestMapping("/login")
	public String login() {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth.getPrincipal() instanceof UserDetails) {
	        return "redirect:/";
	    } 
		
		return "login";
	}
}
