package com.seatapp.seatapp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.seatapp.seatapp.domain.Todo;
import com.seatapp.seatapp.repositories.TodoRespository;
import com.seatapp.seatapp.service.TodoService;

@Service
public class TodoServiceImpl implements TodoService {
	
	@Autowired
	private TodoRespository todoRepository;

	@Override
	public void saveTodo(Todo todo) {
		todoRepository.save(todo);
	}

	@Override
	public void deleteTodoById(Long id) {
		todoRepository.deleteById(id);
	}

	@Override
	public Iterable<Todo> findAllTodo() {
		return todoRepository.findAll();
	}

}
