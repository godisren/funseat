package com.seatapp.seatapp.service;

import com.seatapp.seatapp.domain.Todo;

public interface TodoService {
	
	public void saveTodo(Todo todo);
	
	public void deleteTodoById(Long id);
	
	public Iterable<Todo> findAllTodo();
	
}
